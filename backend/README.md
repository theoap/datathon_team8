# Serverless Geolocation API

Read more about this project [https://medium.com/techulus/build-a-geolocation-api-using-aws-lambda-and-maxmind-25dc3cacc324](https://medium.com/techulus/build-a-geolocation-api-using-aws-lambda-and-maxmind-25dc3cacc324).

## Creating new project

With Serverless Framework v1.5 and later, a new project based on the project template is initialized with the command

```
> sls install -u https://github.com/arjunkomath/serverless-geolocation-api -n myservicename
> cd myservicename
> npm install
```

## License

Copyright (c) 2018 Arjun Komath, licensed for users and contributors under MIT license.
https://github.com/arjunkomath/serverless-geolocation-api/blob/master/LICENSE
