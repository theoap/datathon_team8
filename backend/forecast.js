'use strict';

const DarkSky = require('forecast.io');

const client = new DarkSky({
  APIKey: "eb3f9c39fbe72a5cc713866e33710668"
});

module.exports = (latitude, longitude) => {
  const options = {
    exclude: 'minutely,hourly,daily,flags,alerts',
  };

  return new Promise((resolve, reject) => {
    client.get(latitude, longitude, options, (error, data) => {
      if (error) {
		  console.log(error)
        reject(error);
      } else {
		console.log("hereook")
        resolve(data);
      }
    });
  });
};