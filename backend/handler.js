"use strict"
var request = require('request');
const bluebird = require("bluebird")
const maxmind = require("maxmind")
var haversine = require('haversine-distance')
const openDb = bluebird.promisify(maxmind.open)

let cityLookup,
	countryLookup = null

module.exports.fetchLocationData = async event => {

	var latitude_kachia = 9.8734
	var longitude_kachia = 7.9552
	var latitude_zonkwa = 9.8767
	var longitude_zonkwa = 8.2842
	var latitude_idon = 10.1055
	var longitude_idon = 7.9041
	if (event.source === 'serverless-plugin-warmup') {
		console.log('WarmUP - Lambda is warm!')
		return Promise.resolve('Lambda is warm!')
	}

	if (!cityLookup) {
		cityLookup = await openDb("./GeoLite2-City.mmdb")
	}
	if (!countryLookup) {
		countryLookup = await openDb("./GeoLite2-Country.mmdb")
	}

	let ip, cityData, countryData, rad, a1, a2, b1, b2, dlon, dlat, a, c, R, d1, d2, d3
	try {
		// ip = event.requestContext.identity.sourceIp
		ip = '41.203.87.33'
		cityData = await cityLookup.get(ip)
		countryData = await countryLookup.get(ip)
		var farmer = {
			latitude: cityData.location.latitude,
			longitude: cityData.location.longitude
		}
		var kachia = {
			latitude: latitude_kachia,
			longitude: longitude_kachia
		}
		var zonkwa = {
			latitude: latitude_zonkwa,
			longitude: longitude_zonkwa
		}
		var idon = {
			latitude: latitude_idon,
			longitude: longitude_idon
		}
		d1 = haversine(kachia, farmer)
		d2 = haversine(zonkwa, farmer)
		d3 = haversine(idon, farmer)
	} catch (e) {
		console.log("Lookup failed!", e)
		const response = {
			statusCode: 500,
			body: JSON.stringify({
				success: false,
				error: e
			})
		}
		return Promise.resolve(response)
	}

		const response = {
			statusCode: 200,
			body: JSON.stringify({
				success: true,
				ip: ip,
				city: cityData,
				country: countryData,
				distance1: d1,
				distance2: d2,
				distance3: d3
			})
		}
		return Promise.resolve(response)


}