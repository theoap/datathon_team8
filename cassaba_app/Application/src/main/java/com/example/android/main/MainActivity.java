/*
* Copyright 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.android.main;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.example.android.cardview.R;
import com.example.android.main.connection.CommunicationInterface;
import com.example.android.main.login.LoginActivity;

/**
 * Launcher Activity for the CardView sample app.
 */
public class MainActivity extends Activity {
    private Context mContext;
    public TextView textView;
    RelativeLayout mRelativeLayout;
    private Button mButton;
    static Bundle bundle;

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /**     FOR SOME REASON, JAVA COULD NOT LOAD DATA
         *      FROM API ANYMORE, SO THE METHOD WORKS ONLY
         *      FOR THE INPUT 102.
         */

        /*float distance1 = LoginActivity.data.get(1);
        float distance2 = LoginActivity.data.get(2);
        float distance3 = LoginActivity.data.get(3);
        float foodWaste1 = LoginActivity.data.get(4);
        float foodWaste2 = LoginActivity.data.get(5);
        float foodWaste3 = LoginActivity.data.get(6);*/

        /*Log.d("Data", " " + distance1 + " " + distance2 + " " + distance3+ " " + foodWaste1 + " " +  foodWaste2 + " " + foodWaste3);
        */
        /*
        TextView textView1 = (TextView)findViewById(R.id.cardview1);
        textView1.setText("City value: Kachia\nDistance value: " + Float.toString(distance1) + "\nFood waste value: " + Float.toString(foodWaste1));
        TextView textView2 = (TextView)findViewById(R.id.cardview2);
        textView1.setText("City value: Zomkwa\nDistance value: " + Float.toString(distance2) + "\nFood waste value: " + Float.toString(foodWaste2));
        TextView textView3 = (TextView)findViewById(R.id.cardview3);
        textView1.setText("City value: Idon\nDistance value: " + Float.toString(distance1) + "\nFood waste value: " + Float.toString(foodWaste3));
        */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, CardViewFragment.newInstance())
                    .commit();
        }


    }


}

