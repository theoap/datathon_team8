package com.example.android.main.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.cardview.R;
import com.example.android.main.MainActivity;
import com.example.android.main.connection.CommunicationInterface;
import com.example.android.main.connection.PutUtility;
import org.json.*;
import java.util.HashMap;

public class LoginActivity extends Activity{

    public static HashMap<Integer, Float> data = new HashMap<Integer, Float>();

    CommunicationInterface mCallBack = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Obtain user location
        Button mLocationButton = (Button) findViewById(R.id.get_button);
        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String weather;
                String distance;

                weather = null;
                distance = null;
                PutUtility get = new PutUtility();

                /*try {
                    weather = get.getData("https://api.darksky.net/forecast/eb3f9c39fbe72a5cc713866e33710668/10,8");
                    distance = get.getData("https://ywo4i94ls7.execute-api.us-east-1.amazonaws.com/dev/location");
                    JSONObject obj = new JSONObject(weather);
                    JSONObject obj1 = new JSONObject(distance);
                    float temperature = Float.parseFloat(obj.getJSONObject("currently").getString("temperature"));
                    float distance1 = Float.parseFloat(obj1.getString("distance1"));
                    float distance2 = Float.parseFloat(obj1.getString("distance2"));
                    float distance3 = Float.parseFloat(obj1.getString("distance3"));
                    float celsius = 0.5556F * temperature - 32;
                    float avg_speed = 60000F;
                    float transport_period1= distance1/avg_speed;
                    float transport_period2= distance2/avg_speed;
                    float transport_period3= distance3/avg_speed;

                    EditText text = (EditText) findViewById(R.id.food_quantity);
                    Log.d("dank", text.getText().toString());
                    float weight = Integer.parseInt(text.getText().toString());
                    float Foodloss1 = 100 * ((transport_period1/24F) * weight * 0.3F)/weight;
                    float Foodloss2 = 100 * ((transport_period2/24F) * weight * 0.3F)/weight;
                    float Foodloss3 = 100 * ((transport_period3/24F) * weight * 0.3F)/weight;

                    data.put(1, distance1);
                    data.put(2, distance2);
                    data.put(3, distance3);
                    data.put(4, Foodloss1);
                    data.put(5, Foodloss2);
                    data.put(6, Foodloss3);


                    Log.d("haha", Foodloss1 + Foodloss2 + Foodloss3+ "");
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                System.out.print("done");

                openListPlants();
            }
        });
    }

    public void openListPlants(){
        Intent intent = new Intent();

        intent.setClassName("com.example.android.cardview", "com.example.android.main.MainActivity");
        startActivity(intent);

    }

}
